# README
Demonstrations of odd little curiosities of Java.

## Usage:

This is a maven project so can be run from the command-line with a standard maven command of the form `mvn clean install exec:java`.

## Insect Demo

Demonstrates the different behaviour of runtime and compile-time bindings when accessing member properties directly and via accessor methods.