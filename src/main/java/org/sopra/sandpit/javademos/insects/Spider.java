package org.sopra.sandpit.javademos.insects;

/**
 * Represents a spider.
 */
public class Spider extends Insect
{
  /** Primitive Int Property - Represents the number of legs. */
  public int numberOfLegs = 8;

  /** Primitive Int Property - Represents the number of eyes. */
  public static int numberOfEyes = 8;

  /**
   * Accessor for the named property.
   *
   * @return the numberOfLegs
   */
  public int getNumberOfLegs()
  {
    return numberOfLegs;
  }

  /**
   * Mutator for the named property.
   *
   * @param numberOfLegs The value to set the named property to.
   */
  public void setNumberOfLegs(int numberOfLegs)
  {
    this.numberOfLegs = numberOfLegs;
  }

  /**
   * Accessor for the named property.
   *
   * @return the numberOfLegs
   */
  public static int getNumberOfEyes()
  {
    return numberOfEyes;
  }

}
