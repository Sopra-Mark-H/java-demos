package org.sopra.sandpit.javademos;

import org.sopra.sandpit.javademos.insects.Beetle;
import org.sopra.sandpit.javademos.insects.Insect;
import org.sopra.sandpit.javademos.insects.Spider;

/**
 * An example application for demonstrating Java lessons.
 */
public class App
{
  /**
   * The application's main method.
   *
   * @param args String array of command line params.
   */
  public static void main(String[] args)
  {
    doInsectDemo();
  }
  
  /**
   * Runs the insect demo.
   */
  public static void doInsectDemo()
  {
    Insect insectInsect = new Insect();
    Insect insectSpider = new Spider();
    Insect insectBeetle = new Beetle();
    
    Spider spiderSpider = new Spider();
    Beetle beetleBeetle = new Beetle();
    
    System.out.println("\n================================================================================\n");
    System.out.println(" InsectDemo");
    System.out.println("\n================================================================================\n");
    
    System.out.println(" Accessing values by methods returns what you'd expect...");
    System.out.println(" ");
    System.out.println(" Insect/Insect Number of Legs...: [" + insectInsect.getNumberOfLegs() + "]");
    System.out.println(" Insect/Spider Number of Legs...: [" + insectSpider.getNumberOfLegs() + "]");
    System.out.println(" Insect/Beetle Number of Legs...: [" + insectBeetle.getNumberOfLegs() + "]");
    
    System.out.println(" Spider/Spider Number of Legs...: [" + spiderSpider.getNumberOfLegs() + "]");
    System.out.println(" Beetle/Beetle Number of Legs...: [" + beetleBeetle.getNumberOfLegs() + "]");
    
    System.out.println("\n================================================================================\n");
    System.out.println(" Accessing values directly returns the values in the parent classes!!!...");
    System.out.println(" ");
    System.out.println(" Insect/Insect Number of Legs...: [" + insectInsect.numberOfLegs + "]");
    System.out.println(" Insect/Spider Number of Legs...: [" + insectSpider.numberOfLegs + "]");
    System.out.println(" Insect/Beetle Number of Legs...: [" + insectBeetle.numberOfLegs + "]");
    
    System.out.println(" Spider/Spider Number of Legs...: [" + spiderSpider.numberOfLegs + "]");
    System.out.println(" Beetle/Beetle Number of Legs...: [" + beetleBeetle.numberOfLegs + "]");
    System.out.println("\n================================================================================");
    System.out.println("================================================================================\n");
    System.out.println(" However - look what happens when the method is static (data assumes same number of eye as legs).");
    System.out.println(" ");
    System.out.println(" Accessing values directly returns the values in the parent classes (as before).");
    System.out.println(" ");
    System.out.println(" Insect/Insect Number of Eyes...: [" + insectInsect.numberOfLegs + "]");
    System.out.println(" Insect/Spider Number of Eyes...: [" + insectSpider.numberOfLegs + "]");
    System.out.println(" Insect/Beetle Number of Eyes...: [" + insectBeetle.numberOfLegs + "]");
    
    System.out.println(" Spider/Spider Number of Eyes...: [" + spiderSpider.numberOfLegs + "]");
    System.out.println(" Beetle/Beetle Number of Eyes...: [" + beetleBeetle.numberOfLegs + "]");
    System.out.println("\n================================================================================\n");
    System.out.println(" Accessing by static method...");
    System.out.println(" ");
    System.out.println(" Insect/Insect Number of Eyes...: [" + insectInsect.getNumberOfEyes() + "]");
    System.out.println(" Insect/Spider Number of Eyes...: [" + insectSpider.getNumberOfEyes() + "]");
    System.out.println(" Insect/Beetle Number of Eyes...: [" + insectBeetle.getNumberOfEyes() + "]");
    
    System.out.println(" Spider/Spider Number of Eyes...: [" + spiderSpider.getNumberOfEyes() + "]");
    System.out.println(" Beetle/Beetle Number of Eyes...: [" + beetleBeetle.getNumberOfEyes() + "]");
    System.out.println(" ");
    System.out.println("... So if the methods are static then the base class method will execute even if the instance is of the derived class...");
    System.out.println(" ");
    System.out.println("\n================================================================================\n");

  }
}
